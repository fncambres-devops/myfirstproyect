import os
  import yaml
  import json

  rootdir = '/yamls'

  # Loop over dirs and convert each yaml file to json
  def convert_yamls_to_json(rootdir):
    for d in os.listdir(rootdir):
      dir_path = os.path.join(rootdir, d)
      for yaml_file in os.listdir(dir_path):
        file_ext = os.path.splitext(yaml_file)[1]
        if file_ext in [".yml", ".yaml"]:
          # Read YAML
          with open(os.path.join(dir_path, yaml_file)) as infile:
            data_obj = yaml.load(infile, Loader=yaml.FullLoader)
            json_filename = yaml_file.replace(".yaml", ".json")
            json_filename = yaml_file.replace(".yml", ".json")
            # Write JSON file
            with open(os.path.join(dir_path, json_filename), 'w') as outfile:
              json.dump(data_obj, outfile, indent=2)

  convert_yamls_to_json(rootdir)
